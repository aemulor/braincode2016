Markers = new Mongo.Collection('markers');

var EVENT_TYPE =
    {
        beer: { name: "piwo", icon_path: "/icons/beer.png" },
        boardgame: { name: "planszówki", icon_path: "/icons/boardgame.png" },
        party: { name: "impreza", icon_path: "/icons/party.png" },
        art: { name: "kultura", icon_path: "/icons/art.png" },
        basketball: { name: "koszykówka", icon_path: "/icons/basketball.png" },
        bowling: { name: "kręgle", icon_path: "/icons/bowling.png" },
        cinema: { name: "kino", icon_path: "/icons/cinema.png" },
        coffee: { name: "kawa", icon_path: "/icons/coffee.png" },
        food: { name: "jedzenie", icon_path: "/icons/food.png" },
        soccer: { name: "piłka nożna", icon_path: "/icons/soccer.png" },
        volleyball: { name: "siatkówka", icon_path: "/icons/volleyball.png" },
    }

Router.configure({
    // the default layout
    layoutTemplate: 'MainLayout'
});

Router.route('/', function() {
    // set the layut programmatically
    //this.layout('MainLayout');
    this.render('MainPage');

});

Router.route('/findevent', function() {
    // set the layout based on a reactive session variable
    // this.layout(Session.get('layout') || 'MainLayout');
    this.render('FindEventPage');
});

Router.route('/register', function() {
    this.render('register');
});

Router.route('/login', function() {

    this.render('login');
});

Router.route('/event', function() {

    this.render('EventDetailsPage');
});

// Router.use(function () {
//   if (!this.willBeHandledOnServer())
//     console.error("No route found for url " + JSON.stringify(this.url) + ".");
// });

var lat = ""
var lng = ""

if (Meteor.isClient) {

    Meteor.startup(function() {
        navigator.geolocation.getCurrentPosition(function(position) {
            Session.set('lat', position.coords.latitude);
            Session.set('lon', position.coords.longitude);
            GoogleMaps.load();
        });
    });

    Template.register.events({
        'submit form': function(event) {
            event.preventDefault();
            var email = $('[name=email]').val();
            var password = $('[name=password]').val();
            Accounts.createUser({
                email: email,
                password: password
            }, function(error) {
                if (error) {
                    alert(error.reason); // Output error if registration fails
                } else {
                    Router.go("/"); // Redirect user if registration succeeds
                }
            });
        }
    });

    Template.login.events({
        'submit form': function(event) {
            event.preventDefault();
            var email = $('[name=email]').val();
            var password = $('[name=password]').val();
            Meteor.loginWithPassword(email, password, function(error) {
                if (error) {
                    alert(error.reason);
                } else {
                    Router.go("/");
                }
            });
        }
    });

    Template.MainPage.events({
        'click .logout': function(event) {
            event.preventDefault();
            Meteor.logout();
        }
    });

    Template.MainPage.helpers({
        markeritems: function(){
            return Markers.find(
                {}, {sort: {hotness: -1}}).map(
                    function(mrkr, index) {
                        mrkr.idx = index;
                        return mrkr;
                    });
        }
    });

    Template.listitem.events({
    'click .panel-heading': function(event) {
        var sibl = document.getElementById(this.idx).nextSibling;
        while(sibl.id == null)
            sibl = sibl.nextSibling;
        // hide all panel entries
        $('.panel-collapse.in')
            .collapse('hide');
        // expand selected marker entry
        $("#" + sibl.id).collapse('show');
    }
    });

    Template.markerModalTemplate.events({
        'click #save': function(event) {

            var nazwa = $('#markerNazwa').val()
            var opis = $('#markerOpis').val()
            var data = $('#markerData').val()
            var typ = $('#markerTyp').val()
            var maxosob = $('#markerMaxOsob').val()
            var aktosob = $('#markerAktOsob').val()
            var createdby = "anonymous"
            var currentUser = Meteor.user();
            if (currentUser !== null) {
                createdby = currentUser.emails[0].address;
            }

            Markers.insert({
                lat: lat, lng: lng, nazwa: nazwa,
                opis: opis, data: data, typ: typ,
                maxosob: maxosob, aktosob: aktosob,
                createdby: createdby
            });

            $('#markerModal').modal('hide');
        }
    });

    Template.map.helpers({
        mapOptions: function() {
            if (GoogleMaps.loaded()) {
                return {
                    center: new google.maps.LatLng(Session.get('lat'), Session.get('lon')),
                    zoom: 10
                };
            }
        }
    });

    Template.eventmap.helpers({
        mapOptions: function() {
            if (GoogleMaps.loaded()) {
                return {
                    center: new google.maps.LatLng(Session.get('lat'), Session.get('lon')),
                    zoom: 18
                };
            }
        }
    });

    Template.markerModalTemplate.rendered = function() {
        $('#my-datepicker').datetimepicker();
    }

    Template.eventmap.onCreated(function() {
        GoogleMaps.ready('eventmap', function(map) {
            var infowindow = new google.maps.InfoWindow({
                content: "text"
            });
            var markerInstance = Markers.findOne()
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                origin: new google.maps.LatLng(Session.get('lat'), Session.get('lon')),
                destination: new google.maps.LatLng(markerInstance.lat, markerInstance.lng),
                travelMode: google.maps.DirectionsTravelMode.WALKING,
                unitSystem: google.maps.UnitSystem.METRIC
            };
            directionsService.route(
              directionsRequest,
              function(response, status)
              {
                if (status == google.maps.DirectionsStatus.OK)
                  {
                new google.maps.DirectionsRenderer({
                  map: map.instance,
                  directions: response,
                  suppressMarkers: true,
                  suppressInfoWindows: false,
                  panel: document.getElementById("steps"),
              });
            }});

            var marker = new google.maps.Marker({
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(markerInstance.lat, markerInstance.lng),
                map: map.instance,
                // We store the document _id on the marker in order 
                // to update the document within the 'dragend' event below.
                id: markerInstance._id,
                icon: EVENT_TYPE[markerInstance.typ].icon_path
            });
            var marker_current = new google.maps.Marker({
                  draggable: true,
                  animation: google.maps.Animation.DROP,
                  position: new google.maps.LatLng(Session.get('lat'), Session.get('lon')),
                  map: map.instance,
                  // We store the document _id on the marker in order 
                  // to update the document within the 'dragend' event below.
            });
            infowindow.setContent("Nazwa: " + markerInstance.nazwa + "\nTyp: " + markerInstance.typ + "\n Utworzyl: " + markerInstance.createdby);
            infowindow.open(marker.map, marker);
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent("Nazwa: " + markerInstance.nazwa + "\nTyp: " + markerInstance.typ + "\n Utworzyl: " + markerInstance.createdby);
                infowindow.open(marker.map, marker);
            });
        });
    });


    Template.map.onCreated(function() {
        GoogleMaps.ready('map', function(map) {

            google.maps.event.addListener(map.instance, 'click', function(event) {
                lat = event.latLng.lat()
                lng = event.latLng.lng()
                $('#markerModal').modal('show');
            });
            var infowindow = new google.maps.InfoWindow({
                content: "text"
            });
            var markers = {};

            Markers.find().observe({
                added: function(document) {
                    // Create a marker for this document
                    var marker = new google.maps.Marker({
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        position: new google.maps.LatLng(document.lat, document.lng),
                        map: map.instance,
                        // We store the document _id on the marker in order 
                        // to update the document within the 'dragend' event below.
                        id: document._id,
                        icon: EVENT_TYPE[document.typ].icon_path
                    });

                    // This listener lets us drag markers on the map and update their corresponding document.
                    google.maps.event.addListener(marker, 'dragend', function(event) {
                        Markers.update(marker.id, { $set: { lat: event.latLng.lat(), lng: event.latLng.lng() } });
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent("Nazwa: " + document.nazwa + "\nTyp: " + document.typ + "\n Utworzyl: " + document.createdby);
                        infowindow.open(marker.map, marker);
                        // hide all panel entries
                        $('.panel-collapse.in')
                            .collapse('hide');
                        // expand selected marker entry
                        $("#" + marker.id).collapse('show');
                    });

                    // Store this marker instance within the markers object.
                    markers[document._id] = marker;

                },
                changed: function(newDocument, oldDocument) {
                    markers[newDocument._id].setPosition({ lat: newDocument.lat, lng: newDocument.lng });
                },
                removed: function(oldDocument) {
                    // Remove the marker from the map
                    markers[oldDocument._id].setMap(null);

                    // Clear the event listener
                    google.maps.event.clearInstanceListeners(
                        markers[oldDocument._id]);

                    // Remove the reference to this marker instance
                    delete markers[oldDocument._id];
                }

            });
        });
    });
}

if (Meteor.isServer) {
    Meteor.startup(function() {
        // code to run on server at startup
    });
}
